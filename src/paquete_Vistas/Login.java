package paquete_Vistas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;

import paquete_Controlador.Controlador;
import paquete_Modelos.Estudiante;
import paquete_Modelos.Profesor;

import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JComboBox;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Login {

	private JFrame frame;
	private JTextField usu;
	private JTextField contra;
	private JTextField usuario;
	private JPasswordField con;
	private JPasswordField repetir_con;
	private JTextField matricula;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.DARK_GRAY);
		frame.setBounds(100, 100, 581, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblBienvenidoAlSistema = new JLabel("Bienvenido al Sistema ");
		lblBienvenidoAlSistema.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 18));
		lblBienvenidoAlSistema.setForeground(Color.ORANGE);
		lblBienvenidoAlSistema.setBackground(new Color(255, 200, 0));
		lblBienvenidoAlSistema.setBounds(170, 11, 220, 49);
		frame.getContentPane().add(lblBienvenidoAlSistema);
		
		JPanel panel_registro = new JPanel();
		panel_registro.setBounds(227, 51, 293, 299);
		panel_registro.setVisible(false);
		frame.getContentPane().add(panel_registro);
		panel_registro.setBackground(Color.GRAY);
		panel_registro.setLayout(null);
		
		JLabel label = new JLabel("Usuario");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 18));
		label.setBounds(23, 23, 89, 14);
		panel_registro.add(label);
		
		usuario = new JTextField();
		usuario.setColumns(10);
		usuario.setBorder(null);
		usuario.setBackground(Color.LIGHT_GRAY);
		usuario.setBounds(10, 50, 113, 20);
		panel_registro.add(usuario);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBackground(Color.ORANGE);
		separator_2.setBounds(10, 71, 113, 2);
		panel_registro.add(separator_2);
		
		JLabel label_1 = new JLabel("Contrase\u00F1a");
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 18));
		label_1.setBounds(157, 23, 113, 14);
		panel_registro.add(label_1);
		
		con = new JPasswordField();
		con.setColumns(10);
		con.setBorder(null);
		con.setBackground(Color.LIGHT_GRAY);
		con.setBounds(157, 48, 113, 20);
		panel_registro.add(con);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setBackground(Color.ORANGE);
		separator_3.setBounds(157, 68, 113, 2);
		panel_registro.add(separator_3);
		
		JComboBox rol = new JComboBox();
		rol.setBounds(57, 173, 188, 20);
		rol.addItem("Estudiante");
		rol.addItem("profesor");
		panel_registro.add(rol);
		
		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(repetir_con.getPassword().toString().equals(con.getPassword().toString())){
					
					if(rol.getSelectedItem().equals("Profesor")){
						Profesor p = new Profesor(usu.getText(), matricula.getText());
						
						Controlador.agragarProfesorBD(p, con.getPassword().toString());
					}else{
						Estudiante es = new Estudiante(usu.getText(), matricula.getText());
						
						Controlador.agregarEstudianteBD(es, con.getPassword().toString());
					}
					
				}
				
			}
		});
		btnRegistrar.setFont(new Font("Consolas", Font.BOLD, 16));
		btnRegistrar.setBackground(Color.ORANGE);
		btnRegistrar.setBounds(60, 265, 184, 23);
		panel_registro.add(btnRegistrar);
		
		repetir_con = new JPasswordField();
		repetir_con.setColumns(10);
		repetir_con.setBorder(null);
		repetir_con.setBackground(Color.LIGHT_GRAY);
		repetir_con.setBounds(87, 107, 113, 20);
		panel_registro.add(repetir_con);
		
		JSeparator separator_4 = new JSeparator();
		separator_4.setBackground(Color.ORANGE);
		separator_4.setBounds(87, 127, 113, 2);
		panel_registro.add(separator_4);
		
		JLabel lblRepetirContrasea = new JLabel("Repetir Contrase\u00F1a");
		lblRepetirContrasea.setHorizontalAlignment(SwingConstants.CENTER);
		lblRepetirContrasea.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 18));
		lblRepetirContrasea.setBounds(44, 82, 200, 14);
		panel_registro.add(lblRepetirContrasea);
		
		JLabel lblRol = new JLabel("Rol");
		lblRol.setHorizontalAlignment(SwingConstants.CENTER);
		lblRol.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 18));
		lblRol.setBounds(45, 148, 200, 14);
		panel_registro.add(lblRol);
		
		JLabel lblMatricula = new JLabel("Matricula");
		lblMatricula.setHorizontalAlignment(SwingConstants.CENTER);
		lblMatricula.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 18));
		lblMatricula.setBounds(98, 207, 113, 14);
		panel_registro.add(lblMatricula);
		
		matricula = new JTextField();
		matricula.setColumns(10);
		matricula.setBorder(null);
		matricula.setBackground(Color.LIGHT_GRAY);
		matricula.setBounds(98, 232, 113, 20);
		panel_registro.add(matricula);
		
		JSeparator separator_5 = new JSeparator();
		separator_5.setBackground(Color.ORANGE);
		separator_5.setBounds(98, 252, 113, 2);
		panel_registro.add(separator_5);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		panel.setBounds(21, 121, 196, 205);
		frame.getContentPane().add(panel);
		panel.setLayout(null);

		JPanel panel_inic_ses = new JPanel();
		panel_inic_ses.setBackground(Color.LIGHT_GRAY);
		panel_inic_ses.setForeground(Color.LIGHT_GRAY);
		panel_inic_ses.setBounds(276, 71, 220, 258);
		frame.getContentPane().add(panel_inic_ses);
		panel_inic_ses.setLayout(null);
		
		JLabel lblnoEstasRegistrado = new JLabel("\u00BFNo estas registrado?");
		lblnoEstasRegistrado.setForeground(Color.BLACK);
		lblnoEstasRegistrado.setFont(new Font("Consolas", Font.ITALIC, 15));
		lblnoEstasRegistrado.setBounds(10, 76, 176, 14);
		panel.add(lblnoEstasRegistrado);
		
		JButton registro = new JButton("Registro");
		registro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			panel_registro.setVisible(true);
			}
		});
		registro.setBorder(null);
		registro.setBackground(Color.ORANGE);
		registro.setBounds(34, 100, 126, 43);
		panel.add(registro);
		
		JLabel ok = new JLabel("Usuario");
		ok.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 18));
		ok.setHorizontalAlignment(SwingConstants.CENTER);
		ok.setBounds(66, 25, 89, 14);
		panel_inic_ses.add(ok);
		
		usu = new JTextField();
		usu.setBorder(null);
		usu.setBackground(Color.LIGHT_GRAY);
		usu.setBounds(55, 64, 113, 20);
		panel_inic_ses.add(usu);
		usu.setColumns(10);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(55, 85, 113, 2);
		separator.setBackground(Color.ORANGE);
		panel_inic_ses.add(separator);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a");
		lblContrasea.setBounds(55, 118, 113, 14);
		lblContrasea.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 18));
		lblContrasea.setHorizontalAlignment(SwingConstants.CENTER);
		panel_inic_ses.add(lblContrasea);
		
		contra = new JTextField();
		contra.setColumns(10);
		contra.setBorder(null);
		contra.setBackground(Color.LIGHT_GRAY);
		contra.setBounds(55, 143, 113, 20);
		panel_inic_ses.add(contra);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBackground(Color.ORANGE);
		separator_1.setBounds(55, 163, 113, 2);
		panel_inic_ses.add(separator_1);
		
		JButton btnIniciarSesion = new JButton("Iniciar Sesion");
		btnIniciarSesion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				if(!(usu.getText().trim().equals("") || con.getPassword().toString().trim().equals(""))){
					
					/*
					 * 
					 * Me quede hasta aqui.. por el tiempo
					 * 
					 * 
					 * */
					
				}
				
			}
		});		
		btnIniciarSesion.setFont(new Font("Consolas", Font.BOLD, 16));
		btnIniciarSesion.setBackground(Color.ORANGE);
		btnIniciarSesion.setBounds(23, 211, 184, 23);
		panel_inic_ses.add(btnIniciarSesion);
	}
}
