package paquete_Vistas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JPanel;

public class Vista_Estudiante {

	private JFrame frame;
	private JTable tab_reportes;
	private JTable tab_comentario;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Vista_Estudiante window = new Vista_Estudiante();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Vista_Estudiante() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.DARK_GRAY);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Bienvenido Estudiante");
		lblNewLabel.setForeground(Color.ORANGE);
		lblNewLabel.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 19));
		lblNewLabel.setBounds(210, 21, 226, 29);
		frame.getContentPane().add(lblNewLabel);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.GRAY);
		panel.setBounds(10, 79, 302, 251);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Reportes");
		lblNewLabel_1.setBounds(10, 11, 88, 29);
		panel.add(lblNewLabel_1);
		lblNewLabel_1.setForeground(Color.ORANGE);
		lblNewLabel_1.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 19));
		
		JButton button = new JButton("");
		button.setBackground(Color.BLUE);
		button.setBounds(10, 51, 89, 23);
		panel.add(button);
		button.setForeground(Color.BLUE);
		button.setBorder(null);
		
		tab_reportes = new JTable();
		tab_reportes.setBounds(10, 85, 201, 132);
		panel.add(tab_reportes);
		
		JLabel label = new JLabel("Reportes");
		label.setForeground(Color.ORANGE);
		label.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 19));
		label.setBounds(10, 11, 88, 29);
		panel.add(label);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.LIGHT_GRAY);
		panel_1.setBounds(322, 79, 279, 251);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblComentarios = new JLabel("Comentarios");
		lblComentarios.setForeground(Color.ORANGE);
		lblComentarios.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 19));
		lblComentarios.setBounds(10, 11, 152, 29);
		panel_1.add(lblComentarios);
		
		tab_comentario = new JTable();
		tab_comentario.setBounds(10, 81, 201, 132);
		panel_1.add(tab_comentario);
		frame.setBounds(100, 100, 640, 403);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
