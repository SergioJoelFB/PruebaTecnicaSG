package paquete_Vistas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JTextArea;

public class Vista_Profesor {

	private JFrame frame;
	private JTextField Nombre_estu;
	private JTextField matricula;
	private JTable tabla_estudiantes;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Vista_Profesor window = new Vista_Profesor();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Vista_Profesor() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.DARK_GRAY);
		frame.getContentPane().setLayout(null);
		
		JLabel lblBienvenidoProfesor = new JLabel("Bienvenido Profesor");
		lblBienvenidoProfesor.setForeground(Color.ORANGE);
		lblBienvenidoProfesor.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 19));
		lblBienvenidoProfesor.setBounds(220, 11, 197, 60);
		frame.getContentPane().add(lblBienvenidoProfesor);
		
		JPanel panel_Agregar = new JPanel();
		panel_Agregar.setBackground(Color.GRAY);
		panel_Agregar.setBounds(10, 104, 197, 227);
		frame.getContentPane().add(panel_Agregar);
		panel_Agregar.setLayout(null);
		
		JLabel lblAgregarEstudiante = new JLabel("Agregar Estudiante");
		lblAgregarEstudiante.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 15));
		lblAgregarEstudiante.setForeground(Color.ORANGE);
		lblAgregarEstudiante.setBackground(Color.ORANGE);
		lblAgregarEstudiante.setBounds(27, 11, 144, 14);
		panel_Agregar.add(lblAgregarEstudiante);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 13));
		lblNombre.setForeground(Color.BLACK);
		lblNombre.setBounds(10, 50, 46, 14);
		panel_Agregar.add(lblNombre);
		
		Nombre_estu = new JTextField();
		Nombre_estu.setBorder(null);
		Nombre_estu.setBackground(Color.GRAY);
		Nombre_estu.setBounds(83, 46, 86, 20);
		panel_Agregar.add(Nombre_estu);
		Nombre_estu.setColumns(10);
		
		JSeparator separator = new JSeparator();
		separator.setBackground(Color.ORANGE);
		separator.setForeground(Color.ORANGE);
		separator.setBounds(85, 80, 81, -4);
		panel_Agregar.add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBackground(Color.ORANGE);
		separator_1.setBounds(83, 68, 86, 8);
		panel_Agregar.add(separator_1);
		
		JLabel Matricula = new JLabel("Matricula");
		Matricula.setForeground(Color.BLACK);
		Matricula.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 13));
		Matricula.setBounds(10, 102, 72, 14);
		panel_Agregar.add(Matricula);
		
		matricula = new JTextField();
		matricula.setColumns(10);
		matricula.setBorder(null);
		matricula.setBackground(Color.GRAY);
		matricula.setBounds(83, 98, 86, 20);
		panel_Agregar.add(matricula);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBackground(Color.ORANGE);
		separator_2.setBounds(83, 120, 86, 8);
		panel_Agregar.add(separator_2);
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 15));
		btnAgregar.setBackground(Color.ORANGE);
		btnAgregar.setBounds(52, 170, 89, 23);
		panel_Agregar.add(btnAgregar);
		
		JPanel panel_editar = new JPanel();
		panel_editar.setBackground(Color.LIGHT_GRAY);
		panel_editar.setBounds(220, 104, 208, 227);
		frame.getContentPane().add(panel_editar);
		panel_editar.setLayout(null);
		
		JLabel lblEditarEstudiante = new JLabel("Estudiantes");
		lblEditarEstudiante.setForeground(Color.BLACK);
		lblEditarEstudiante.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 15));
		lblEditarEstudiante.setBackground(Color.ORANGE);
		lblEditarEstudiante.setBounds(60, 11, 88, 14);
		panel_editar.add(lblEditarEstudiante);
		
		tabla_estudiantes = new JTable();
		tabla_estudiantes.setBounds(10, 36, 188, 180);
		panel_editar.add(tabla_estudiantes);
		
		JPanel panel_comentario = new JPanel();
		panel_comentario.setBackground(Color.GRAY);
		panel_comentario.setBounds(441, 104, 188, 227);
		frame.getContentPane().add(panel_comentario);
		panel_comentario.setLayout(null);
		
		JLabel lblAgregarComentario = new JLabel("Agregar Comentario");
		lblAgregarComentario.setForeground(Color.ORANGE);
		lblAgregarComentario.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 15));
		lblAgregarComentario.setBackground(Color.ORANGE);
		lblAgregarComentario.setBounds(23, 11, 144, 14);
		panel_comentario.add(lblAgregarComentario);
		
		JLabel label = new JLabel("Matricula");
		label.setForeground(Color.BLACK);
		label.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 13));
		label.setBounds(8, 52, 72, 14);
		panel_comentario.add(label);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBorder(null);
		textField.setBackground(Color.GRAY);
		textField.setBounds(81, 48, 86, 20);
		panel_comentario.add(textField);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setBackground(Color.ORANGE);
		separator_3.setBounds(81, 70, 86, 8);
		panel_comentario.add(separator_3);
		
		JLabel lblComentario = new JLabel("Comentario");
		lblComentario.setForeground(Color.BLACK);
		lblComentario.setFont(new Font("Consolas", Font.BOLD | Font.ITALIC, 13));
		lblComentario.setBounds(52, 112, 72, 14);
		panel_comentario.add(lblComentario);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(8, 136, 170, 80);
		panel_comentario.add(textArea);
		frame.setBounds(100, 100, 656, 411);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
