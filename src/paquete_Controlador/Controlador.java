package paquete_Controlador;

import java.awt.HeadlessException;
import java.sql.*;

import javax.swing.JOptionPane;

import paquete_Modelos.Estudiante;
import paquete_Modelos.Profesor;
import paquete_Vistas.Vista_Profesor;

public class Controlador {
	
	public Controlador(){
		
	}
	
	public static void agragarProfesorBD(Profesor p, String clave){
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			
			String con = "jdbc:mysql://127.0.0.1:3306/sis_calificaciones, '', 'root' ";
			 
			Connection c = DriverManager.getConnection(con);
			 
			String agregar = "INSERT INTO prefesor VALUES(" + p.getNombre() + "," + p.getMateria() + "," + clave + ")";
			 
			Statement st = c.createStatement();
			
			st.executeUpdate(agregar);
				 
			c.close();
			
			JOptionPane.showConfirmDialog(null, "Profesor registrado correctamente");
				 
		} catch (ClassNotFoundException e) {
	
			e.printStackTrace();
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
	
	}
	
	public static void agregarEstudianteBD(Estudiante es, String clave){
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			
			String con = "jdbc:mysql://localhost:3306/sis_calificaciones";
			 
			Connection c = DriverManager.getConnection(con);
			 
			String agregar = "INSERT INTO estudiante VALUES(" + es.getNombre() + "," + es.getMatricula() + ", "+ clave +")";
			 
			Statement st = c.createStatement();
			
			st.executeUpdate(agregar);
			
			c.close();
				 
				 
		} catch (ClassNotFoundException e) {
	
			e.printStackTrace();
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
	}
	
	public static Vista_Profesor LoguearProfesor(Profesor p){
		
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			
			java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ges_bd", "root", "");

			String sql = "SELECT nombre, materia FROM profesor WHERE usuario = ? AND contra = ? ";
			
			PreparedStatement ps;
			ps = con.prepareStatement(sql);
			ps.setString(1, p.getNombre());
			ps.setString(2, p.getMateria());
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				
				JOptionPane.showMessageDialog(null, "Bienvenido al sistema");
				
				try {
					
					return new Vista_Profesor();
					
				} catch (Exception e1) {
					e1.getStackTrace();
				}
		
			}else{
				JOptionPane.showMessageDialog(null, "El usuario no existe");
		
			}
	
			con.close();
	
		}catch(ClassNotFoundException | HeadlessException | SQLException e){
			e.printStackTrace();
		}
		return null;
		
	}
	
}
