package paquete_Modelos;

public class Estudiante {

	private String nombre;
	
	private String matricula;
	
	public Estudiante(String n, String m){
		this.nombre = n;
		this.matricula = m;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
}
